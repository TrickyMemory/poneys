<?php
require_once 'src/Poneys.php';

class Test extends \PHPUnit_Framework_TestCase {
	
	public function test_addPoneyFromField(){
		//Setup
		$Poneys = new Poneys();

                // Action
                $Poneys->addPoneyFromField(3);

                //Assert
 		this->assertEquals($Poneys->getCount(),11);	
	}      
}
?>
