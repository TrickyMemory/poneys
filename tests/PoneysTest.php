<?php
require_once 'src/Poneys.php';

class PoneysTest extends \PHPUnit_Framework_TestCase {
	
    /**
     * @dataProvider removeProvider
     */
	public function test_removePoneyFromField($number, $expected) {
		// Setup
		$Poneys = new Poneys();

		//Action
		$Poneys->removePoneyFromField($number);

		//Assert
		$this->assertEquals($expected, $Poneys->getCount());
	}

	public function removeProvider(){
		return array(
			array(3,5),
			array(5,3),
		) ;
	}

	    /**
	     * @expectedException     Exception
	     */
	public function test_removePoneyFromFieldException(){
		//Setup
		$Poneys = new Poneys();

		//Action 
		$Poneys->removePoneyFromField(10);

		//Assert
		$this->assertGreaterThanOrEqual(0, $Poneys->getCount());
	}

	public function test_addPoneyFromField(){
		//Setup
		$Poneys = new Poneys();

                // Action
                $Poneys->addPoneyFromField(3);

                //Assert
                $this->assertEquals(11, $Poneys->getCount());
	}

	public function test_getNames(){
		$Poneys = $this ->getMockBuilder('Poneys')
				->setMethods(array('getNames'))
				->getMock();
	}

	public function test_placeInField(){
		$Poneys=new Poneys();
		$this->assertTrue($Poneys->placeInField());

		$Poneys->setCount(15);
		$this->assertFalse($Poneys->placeInField());
	}

	public function SetUp(){
		//Setup
		$Poneys = new Poneys();
		
		//Action
		$Poneys->setCount(12);

		//Assert
		$this->assertEquals(12, $Poneys->getCount());
	}

	public function TearDown(){
		//Setup
		$Poneys = NULL;
	}
}
?>
