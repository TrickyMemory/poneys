<?php 
class Poneys {
	private $count = COUNT2;
	private $name = array();
	private $fieldSize = FIELDSIZE;

	public function getCount() {
		return $this->count;
	}

	public function setCount($number){
		$this->count = $number;
	}

	public function setFieldSize($number){
		$this->fieldSize = $number;
	}

	public function removePoneyFromField($number) {
		$this->count -= $number;
		if ($this->count <0){
			throw new Exception('Nombre de poneys négatifs');
		}
	}


	public function addPoneyFromField($number){
		$this->count += $number;
	}

	public function getNames() {
		return $this->name;
	}

	public function placeInField(){
		if( $this->count < ($this->fieldSize)){
			return true;
		}
		else { return false; }
	} 
}
?>
